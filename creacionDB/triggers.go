package creacionDB

import (
		"log"
		"database/sql"
		"fmt"
		_ "github.com/lib/pq"
)

//var db *sql.DB

/*
Este trigger crea alertas automaticas por posibles fraudes
-Se genera una alerta cuando se ingresa un rechazo
-Se genera una alerta cuando una tarjeta registra dos compras en un
lapso menor de un minuto en comercios distintos ubicados en un mismo
codigo postal
-Se genera una alerta si se realizan dos compras con la misma tarjeta
en un lapso menor a cinco munutos en distintos codigos postales
-Se genera una alerta si una tarjeta registra dos rechazos por exceso
de limite de consumo en el mismo dia. La tarjeta es suspendida
*/

func CreacionAlertaRechazo(db *sql.DB){
	fmt.Println("Comienza a crearse el trigger alertaRechazo()\n")
	_, err := db.Exec(
		`
		create or replace function alertaRechazo()
		returns trigger as $$
			begin
				insert into alerta (nrotarjeta,fecha,nrorechazo,codalerta,descripcion) values 
				(new.nrotarjeta, new.fecha, new.nrorechazo, 0,new.motivo);
				return new;
			end;
			$$ language plpgsql;
				
		create trigger alertaRechazoT
		after insert on rechazo
		for each row
		execute procedure alertaRechazo()
		;`)
		if err != nil{
			log.Fatal(err)
		}
}

func CreacionAlertaLapsoUnMin(db *sql.DB){
		fmt.Println("Comienza a crearse el trigger alertaLapsoUnMin()\n")
			_, err := db.Exec(
				`
				create or replace function alertaLapsoUnMin()
				returns trigger as $$
					declare
						fechaVieja timestamp;
						diferencia interval;
						lapso interval;
					begin
						if new.nrooperacion > 1 then
							select fecha into fechaVieja from 
							compra where (nrooperacion = (new.nrooperacion-1));
							diferencia:= (now()::timestamp)-fechaVieja;
							lapso:='0 day 00:01:00';
							if diferencia < lapso then
								insert into alerta (nrotarjeta,fecha,codalerta,descripcion)
								 values (new.nrotarjeta, new.fecha, 1,
								 'Se realizaron dos compras en menos de un lapso de 1 min');
							end if;
						end if;
						return new;  
					end;
					$$ language plpgsql;
						
				create trigger alertaLapsoUnMinT
				after insert on compra
				for each row
				execute procedure alertaLapsoUnMin()
				;`)
				if err != nil{
					log.Fatal(err)
				}
}
