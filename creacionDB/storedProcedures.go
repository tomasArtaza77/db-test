package creacionDB

import (	
	"fmt"
	"log"
	"database/sql"
)


//var db *sql.DB

/*
Al momento de realizar una compra, autorizarCompra() se encarga de validar, generar rechazos o alertas.
Incluir la lógica que reciba los datos de una
compra —número de tarjeta, código de seguridad, número de comercio y monto— y
que devuelva true si se autoriza la compra ó false si se rechaza.
*/
func AutorizarCompra(db *sql.DB)  {
	fmt.Println("Comienzan a autorizarse las compras.\n")
	_, err := db.Query(
		`
			create or replace function autorizar_compra(_nrotarjeta char(16), _codseguridad char(4), _nrocomercio int, _monto decimal(7,2)) returns bool as $$
			declare

				pendiente decimal(8,2);
				limiteTarjeta decimal(8,2);
				vencimientoTarjeta int;
				_nrooperacion int;

			begin

				perform * from tarjeta t where t.nrotarjeta = _nrotarjeta and t.estado = 'vigente';
				if (not found) then
					return false; 
				end if;

				
				perform * from tarjeta t where t.nrotarjeta = _nrotarjeta and t.codseguridad = _codseguridad;
				if (not found) then
					return false; 
				end if;

				pendiente := (select sum(monto) from compra where nrotarjeta = _nrotarjeta and pagado = False);
				limiteTarjeta := (select limitecompra from tarjeta where nrotarjeta = _nrotarjeta);
				if (pendiente is null and _monto > limiteTarjeta or 
					pendiente is not null and (pendiente + _monto) > limiteTarjeta) then
					return false;
				end if;

				select validahasta into vencimientoTarjeta from tarjeta t where t.nrotarjeta = _nrotarjeta;
				if (vencimientoTarjeta > current_date) then
					return false;
				end if;

				perform * from tarjeta t where t.nrotarjeta = _nrotarjeta and t.estado = 'suspendida';
				if (found) then 
					return false;
				end if;


				perform * from compra c where c.nrooperacion is not null;
				if (found) then
					select max(nrooperacion)+1 into _nrooperacion from compra;
				else
					_nrooperacion:= 1;
				end if;
				insert into compra(nrooperacion, nrotarjeta, nrocomercio, fecha, monto, pagado)
					values (_nrooperacion, _nrotarjeta, _nrocomercio, current_timestamp, _monto, true);
				return true;

			end;
			$$ language plpgsql
			;`)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Fin de autorizar compra.\n")

}
