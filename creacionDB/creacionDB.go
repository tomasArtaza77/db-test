package creacionDB

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"log"
)

type cliente struct {
	nrocliente int
	nombre, apellido, domicilio string
	telefono string
}

var db *sql.DB

func CreateDatabase() {

	fmt.Println("Comienza a crearse la Base de datos\n")
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=postgres sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	_, err = db.Exec(`drop database if exists tp`)
	if err != nil {
		log.Fatal(err)
	}
	_, err = db.Exec(`create database tp`)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Se creo la Base de datos\n")

}

func CreateTables(db *sql.DB) {
	fmt.Println("Comienzan a crearse las tablas\n")
	_, err := db.Exec(`create table cliente(
					   		nrocliente int,
					   		nombre text,
					   		apellido text,
					   		domicilio text,
					   		telefono char(12)
					   );
					   create table tarjeta(
					   		nrotarjeta char(16),
					   		nrocliente int,
					   		validadesde char(6), 
					   		validahasta char(6),
					   		codseguridad char(4),
					   		limitecompra decimal(8,2),
					   		estado char(10)
					   );
					   create table comercio(
					   		nrocomercio int,
					   		nombre text,
					   		domicilio text,
					   		codigopostal char(8),
					   		telefono char(12)
					   );
					   create table compra(
					   		nrooperacion int GENERATED ALWAYS AS IDENTITY,
					   		nrotarjeta char(16),
					   		nrocomercio int,
					   		fecha timestamp,
					   		monto decimal(7,2),
					   		pagado boolean
					   );
					   create table rechazo(
					   		nrorechazo int GENERATED ALWAYS AS IDENTITY,
					   		nrotarjeta char(16),
					   		nrocomercio int,
					   		fecha timestamp,
					   		monto decimal(7,2),
					   		motivo text
					   );
					   create table cierre(
					   		año int,
					   		mes int,
					   		terminacion int,
					   		fechainicio date,
					   		fechacierre date,
					   		fechavto date
					   );
					   create table cabecera(
					   		nroresumen int,
					   		nombre text,
					   		apellido text,
					   		domicilio text,
					   		nrotarjeta char(16),
					   		desde date,
					   		hasta date,
					   		vence date,
					   		total decimal(8,2)
					   );
					   create table detalle(
					   		nroresumen int,
					   		nrolinea int,
					   		fecha date,
					   		nombrecomercio text,
					   		monto decimal(7,2)
					   );
					   create table alerta(
					   		nroalerta int GENERATED ALWAYS AS IDENTITY,
					   		nrotarjeta char(16),
					   		fecha timestamp,
					   		nrorechazo int,
					   		codalerta int, 
					   		descripcion text
					   );
					   create table consumo(
					   		nrotarjeta char(16),
					   		codseguridad char(4),
					   		nrocomercio int,
					   		monto decimal(7,2)
					   );`)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Se crearon las tablas\n")

}

func CreacionPK(db *sql.DB) {
	fmt.Println("Comenzaron a crearse las PKs\n")
	_, err := db.Exec(`alter table cliente add constraint cliente_pk primary key (nrocliente);
					  alter table tarjeta add constraint tarjeta_pk primary key (nrotarjeta);
		              alter table comercio add constraint comercio_pk primary key (nrocomercio);
		              alter table compra add constraint compra_pk primary key (nrooperacion);
		              alter table rechazo add constraint rechazo_pk primary key (nrorechazo);
		              alter table cierre add constraint cierre_pk primary key (año, mes, terminacion);
		              alter table cabecera add constraint cabecera_pk primary key (nroresumen);
		              alter table detalle add constraint detalle_pk primary key (nrolinea);
		              alter table alerta add constraint alerta_pk primary key (nroalerta);`)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Se crearon las PKs\n")
}

func CreacionFK(db *sql.DB) {
	fmt.Println("Comenzaron a crearse las FKs\n")
	_, err := db.Exec(`alter table tarjeta add constraint tarjeta_nrocliente_fk foreign key (nrocliente) references cliente(nrocliente); 
					  alter table compra add constraint compra_nrotarjeta_fk foreign key (nrotarjeta) references tarjeta(nrotarjeta); 
					  alter table compra add constraint compra_nrocomercio_fk foreign key (nrocomercio) references comercio(nrocomercio); 
				      alter table rechazo add constraint rechazo_nrotarjeta_fk foreign key (nrotarjeta) references tarjeta(nrotarjeta); 
					  alter table rechazo add constraint rechazo_nrocomercio_fk foreign key (nrocomercio) references comercio(nrocomercio); 
		              alter table cabecera add constraint cabecera_nrotarjeta_fk foreign key (nrotarjeta) references tarjeta(nrotarjeta); 
		              alter table detalle add constraint detalle_nroresumen_fk foreign key (nroresumen) references cabecera(nroresumen); 
					  alter table alerta add constraint alerta_nrotarjeta_fk foreign key (nrotarjeta) references tarjeta(nrotarjeta); 
					  alter table alerta add constraint alerta_nrorechazo_fk foreign key (nrorechazo) references rechazo(nrorechazo);`)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Se crearon las FKs\n")
}

func EliminarPK(db *sql.DB) {
	fmt.Println("Comenzaron a eliminarse las PKs\n")
	_, err := db.Exec(`
		alter table cliente drop constraint cliente_pk;
		alter table tarjeta drop constraint tarjeta_pk;
		alter table comercio drop constraint comercio_pk;
		alter table compra drop constraint compra_pk;
		alter table rechazo drop constraint rechazo_pk;
		alter table cierre drop constraint cierre_pk;
		alter table cabecera drop constraint cabecera_pk;
		alter table detalle drop constraint detalle_pk;
		alter table alerta drop constraint alerta_pk;
	`)

	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Se eliminaron las PKs\n")
}

func EliminarFK(db *sql.DB) {
	fmt.Println("Comenzaron a eliminarse las FKs\n")
	_, err := db.Exec(`
		alter table tarjeta drop constraint tarjeta_nrocliente_fk;
		alter table compra drop constraint compra_nrotarjeta_fk;
		alter table compra drop constraint compra_nrocomercio_fk;
		alter table rechazo drop constraint rechazo_nrotarjeta_fk;
		alter table rechazo drop constraint rechazo_nrocomercio_fk;
		alter table cabecera drop constraint cabecera_nrotarjeta_fk;
		alter table detalle drop constraint detalle_nroresumen_fk;
		alter table alerta drop constraint alerta_nrotarjeta_fk;
		alter table alerta drop constraint alerta_nrorechazo_fk;
	`)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Se eliminaron las FKs\n")
}

func InsertValues(db *sql.DB) {
	_, err := db.Exec(`
		insert into cliente values
			(1, 'Juan', 'Castro', 'Juan José Paso 1950, Ciudadela, Buenos Aires', '11 8943-6182'),
			(2, 'Pablo', 'Sosa', 'Directorio 1040, Tortuguitas, Buenos Aires', '11 8492-6749'),
			(3, 'Pedro', 'Gomez', 'Enciso 1579, Tigre, Buenos Aires', '11 6437-1601'),
			(4, 'Silvina', 'Perez', 'España 1350, San Miguel, Buenos Aires', '11 0673-9572'),
			(5, 'Manuel', 'Arriola', 'Oribe 2432, José C. Paz, Buenos Aires', '11 9270-6173'),
			(6, 'Cristina', 'Garcia', 'Pampa 945, Pilar, Buenos Aires', '11 4915-4391'),
			(7, 'Liliana', 'Alvarez', 'Suipacha 847, Merlo, Buenos Aires', '11 3481-9564'),
			(8, 'José', 'Sanchez', 'Alsina 426, Moreno, Buenos Aires', '11 2465-0494'),
			(9, 'Ignacio', 'Torres', 'Yatay 789, Morón, Buenos Aires', '11 7294-0653'),
			(10, 'Norma', 'Ruiz', 'Haedo 1801, Muñiz,Buenos Aires', '11 5042-6186'),
			(11, 'Martina', 'Lopez', 'Talcahuano 3295, Los Polvorines, Buenos Aires', '11 6172-4658'),
			(12, 'Miguel', 'Artaza', 'Yatasto 1800, Grand Bourg, Buenos Aires', '11 9452-6143'),
			(13, 'Gustavo', 'Martinez', 'Balbastro 2545, Don Torcuato, Buenos Aires', '11 7250-4691'),
			(14, 'María', 'Leiva', 'Cavassa 2887, Caseros, Buenos Aires', '11 5016-9482'),
			(15, 'Ana', 'Romero', 'Belgrano 1645, San Martín, Buenos Aires', '11 7751-0619'),
			(16, 'Carlos', 'Gonzalez', 'Don Bosco 830, San Isidro, Buenos Aires', '11 2006-4361'),
			(17, 'Camila', 'Rodriguez', 'Paunero 1721, San Miguel, Buenos Aires', '11 5064-7312'),
			(18, 'Marcelo', 'Diaz', 'Moine 1828, Bella Vista, Buenos Aires', '11 0643-9785'),
			(19, 'Luis', 'Fernandez', 'Galicia 1479, Castelar, Buenos Aires', '11 3461-5016'),
			(20, 'Lucía', 'Acosta', 'Pizarro 3401, Trujui, Buenos Aires', '11 2469-8941');
			
		insert into comercio values
			(1, 'Motopaz Mensajeria', 'Sta. Rosa 1535, Castelar, Buenos Aires', 'B1712LTG', '11 2676-5201'),
			(2, 'Almaraz Ferreteria', 'Av. Perón 15222, San Miguel, Buenos Aires', 'B1663GHR', '11 4061-8470'),
			(3, 'Cristiana Librería', 'Sanabria 2885, Villa de Mayo, Buenos Aires', 'B1614ACM', '11 5648-1156'),
			(4, 'Bentosan Refrigeración', 'Zuviría 5083, José C. Paz, Buenos Aires', 'B1665GKC', '11 0114-0180'),
			(5, 'Garro Cotillon', 'Moreno 90, Pilar, Buenos Aires', 'B1629INB', '11 8064-8406'),
			(6, 'Marita Heladeria', 'Conesa 801, Muñiz, Buenos Aires', 'B1662DER', '11 0634-5942'),
			(7, 'Fratelli Sanitarios', 'Serrano 1771, San Miguel, Buenos Aires', 'B1663GUM', '11 6385-4188'),
			(8, 'Stella Restaurant', 'Thames 1327, Trujui, Buenos Aires', 'B1736HHI', '11 4873-2198'),
			(9, 'La Palabra Fiambreria', 'Colombia 699, Moreno, Buenos Aires', 'B1743GVO', '11 7452-0614'),
			(10, 'Vintage Gimnasio', 'Riobamba 517, Merlo, Buenos Aires', 'B1722MUK', '11 6842-6830'),
			(11, 'Boso Cafetería', 'Guaminí 5443, Caseros, Buenos Aires', 'B1678BOH', '11 3206-5645'),
			(12, 'Diega Kiosko', 'Belgrano 529, Morón, Buenos Aires', 'B1708IFK', '11 7864-5264'),
			(13, 'Tresge Tecnología', 'Felipe Llavallol 3973, Tortuguitas, Buenos Aires', 'B1667CAP', '11 4786-7809'),
			(14, 'Tawe Veterinaria', 'Defensa 2172, El Talar, Buenos Aires', 'B1618BAP', '11 8164-8935'),
			(15, 'Leon Supermercado', 'Juncal 1117, Tigre, Buenos Aires', 'B1648FFC', '11 6804-8024'),
			(16, 'Huellita Indumentaria', 'Sourdeaux 2021, Bella Vista, Buenos Aires', 'B1661GYB', '11 0256-6870'),
			(17, 'Barrufaldi Verduleria', 'Portinari 1570, Del Viso, Buenos Aires', 'B1669BBL', '11 9847-8621'),
			(18, 'Leal Carnicería', 'Luis Vernet 1730, Grand Bourg, Buenos Aires', 'B1615IVD', '11 4868-2527'),
			(19, 'Comora Polleria', 'Hiroshima 918, Pablo Nogués, Buenos Aires', 'B1616BWD', '11 1041-5841'),
			(20, 'Nochera Corralon', 'Platón 5855, José C. Paz, Buenos Aires', 'B1665HUO', '11 9189-8765');

		insert into tarjeta values
			('4552416565115665', 1, '201906', '202305', '1264', 100000.00, 'vigente'),
			('4151556165165154', 2, '201905', '202503', '8192', 160000.00, 'vigente'),
			('4624049579509714', 3, '201904', '202507', '7361', 180000.00, 'vigente'),
			('4892415698441915', 4, '202007', '202611', '8061', 150000.00, 'vigente'),
			('5961561661841856', 5, '201908', '202604', '5040', 90000.00, 'vigente'),
			('5327354601366106', 6, '202001', '202606', '4154', 200000.00, 'vigente'),
			('5815616598184537', 7, '201902', '202609', '4606', 110000.00, 'vigente'),
			('5915174707860760', 8, '201911', '202512', '4980', 100000.00, 'vigente'),			
			('4192416601033245', 9, '201801', '202212', '4891', 270000.00, 'vigente'),
			('4651084684080351', 10, '201812', '202304', '2864', 120000.00, 'vigente'),
			('4978410897106307', 11, '201806', '202506', '8066', 230000.00, 'vigente'),
			('4064709650768654', 12, '201805', '202411', '6551', 300000.00, 'vigente'),
			('4706450163386840', 13, '201806', '202303', '7060', 220000.00, 'vigente'),
			('5017405340562570', 14, '201803', '202307', '0864', 190000.00, 'vigente'),
			('5741802107176804', 15, '201810', '202409', '7943', 130000.00, 'vigente'),

			('5489308662239829', 16, '201805', '202306', '1378', 170000.00, 'vigente'),
			('5876248622561468', 16, '201908', '202504', '7868', 260000.00, 'vigente'),
			('4789392605754355', 17, '202006', '202603', '9606', 40000.00, 'vigente'),
			('5786226610262526', 17, '201807', '202405', '8429', 80000.00, 'vigente'),

			('4982156635453185', 18, '201503', '202011', '3923', 160000.00, 'vigente'), /*expirada*/

			('5321451616585264', 19, '201910', '202308', '6510', 340000.00, 'suspendida'),
			('5687006150348516', 20, '201809', '202601', '9191', 230000.00, 'anulada');			
	`)
	if err != nil {
		log.Fatal(err)
	}  

	fmt.Println("Se realizaron los inserts a las tablas cliente y comercio\n")
}

func VerTablaClientes(db *sql.DB) {
	rows, err := db.Query(`select * from cliente`)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	var c cliente
	for rows.Next() {
		if err := rows.Scan(&c.nrocliente, &c.nombre, &c.apellido, &c.domicilio, &c.telefono); err != nil {
			log.Fatal(err)
		} 
		fmt.Printf("%v %v %v %v %v\n", c.nrocliente, c.nombre, c.apellido, c.domicilio, c.telefono)			
	}
	if err = rows.Err(); err != nil {
		log.Fatal(err)
	}
}
