package boltDB

import (
	"encoding/json"
	"fmt"
	bolt "go.etcd.io/bbolt"
	"log"
	"strconv"
)

type Cliente struct {
	Nrocliente int
	Nombre string
	Apellido string
	Domicilio string
	Telefono string
}

type Tarjeta struct {
	Nrotarjeta string
	Nrocliente int
	Validadesde string
	Validahasta string
	Codseguridad string
	Limitecompra float64
	Estado string
}

type Comercio struct {
	Nrocomercio int
	Nombre string
	Domicilio string
	Codigopostal string
	Telefono string
}

type Compra struct {
	Nrooperacion int
	Nrotarjeta string
	Nrocomercio int
//	fecha 
	Monto float64
	Pagado bool
}

func CrearBoltdb() {
	db, err := bolt.Open("bolt.db", 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	
	juan := Cliente{1, "Juan", "Castro", "Juan José Paso 1950, Ciudadela, Buenos Aires", "11 8943-6182"}
	data, err := json.Marshal(juan)
	if err != nil {
		log.Fatal(err)
	}

	CreateUpdate(db, "cliente", []byte(strconv.Itoa(juan.Nrocliente)), data)

	resultado, err := ReadUnique(db, "cliente", []byte(strconv.Itoa(juan.Nrocliente)))

	fmt.Printf("%s\n", resultado)
}

func CreateUpdate(db *bolt.DB, bucketName string, key []byte, val []byte) error {
	// abre transaccion de escritura
	tx, err := db.Begin(true)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	b, err := tx.CreateBucketIfNotExists([]byte(bucketName))
	if err != nil {
		return err
	}

	err = b.Put(key, val)
	if err != nil {
		return err
	}

	// cierra transaccion
	if err := tx.Commit(); err != nil {
		return err
	}

	return nil
}

func ReadUnique(db *bolt.DB, bucketName string, key []byte) ([]byte, error) {
	var buf []byte

	// abre una transaccion de lectura
	err := db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(bucketName))
		buf = b.Get(key)
		return nil
	})

	return buf, err
}


