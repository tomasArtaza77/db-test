package main

import (
	"artaza-casco-ramirez-velazquez-tp/creacionDB"
	"database/sql"
	_ "github.com/lib/pq"
	"log"
)

func main() {
	creacionDB.CreateDatabase()

	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=tp sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	creacionDB.CreateTables(db)

	creacionDB.CreacionPK(db)
	creacionDB.CreacionFK(db)

//	creacionDB.EliminarFK(db)
//	creacionDB.EliminarPK(db)

	creacionDB.InsertValues(db)
//	creacionDB.VerTablaClientes(db)

	creacionDB.CreacionAlertaRechazo(db)
	creacionDB.CreacionAlertaLapsoUnMin(db)

	creacionDB.AutorizarCompra(db)
}
